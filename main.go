package main

import (
	"net/http"
	"fmt"
	"os"
	"github.com/pkg/errors"
	"net/smtp"
	"time"
)

type Person struct {
	name string
	email string
}

type Team []Person


var URLS = []string{"https://www.avenuecode.com", "https://www.avenuecode.com.br",}
var client = &http.Client{}
var smtpPassword string

var maxAttepts = 5

var websiteTeam = Team{
	Person{"Bernardo Vale", "bernardosilveiravale@gmail.com"},
	Person{"Bernardo Vale", "bvale@avenuecode.com"},
	Person{"Pedro Cardoso", "pcolen@avenuecode.com"},
	Person{"Andre Almeida", "aalmeida@avenuecode.com"},
}

func main(){
	var err error
	var i int

	//try five times just in case
	for i=0; i<maxAttepts; i++{
		err = checkStatus(URLS)
		if err == nil{
			os.Exit(exit(nil))
		}
		// Last check. don't need to wait
		if i < maxAttepts-1{
			fmt.Println("Got error, trying again, Attempt:", i+1)
			time.Sleep(time.Second*5)
		}
	}
	os.Exit(exit(err))
}


func checkStatus(urls []string) (err error){
	var r *http.Response
	for _, url := range urls{
		if r, err = client.Get(url); err !=nil{
			return err
		}
		if r.StatusCode != http.StatusOK{
			return errors.New(fmt.Sprintf("Request %s receive statusCode:%d", url, r.StatusCode))
		}
	}
	return nil
}


func exit(err error) int{
	if err != nil{
		err = mail(err.Error())
		if err !=nil{
			fmt.Println(err.Error())
		}
		return 1
	}
	fmt.Println("Website it's ok!")
	return 0
}

func mailMessage(body string) (msg string){
	to := ""
	for _, p := range websiteTeam {
		to += fmt.Sprintf("%s <%s>, ", p.name, p.email)
	}
	to = to[0:len(to)-2]
	from := "Website Operations <webmaster.avenuecode.com>"
	subject := "Website Monitoring Issues\n\n"
	msg = fmt.Sprintf("From: %s\nTo: %s\nSubject: %s\n %s", from, to, subject, body)
	return msg
}
func mail(body string) (err error){
	fmt.Println("Sending email...")
	from := "webmaster@avenuecode.com"

	teamEmail := []string{}

	for _, p := range websiteTeam{
		teamEmail = append(teamEmail, p.email)
	}
	msg := mailMessage(body)

	err = smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, smtpPassword, "smtp.gmail.com"),
		from, teamEmail, []byte(msg))

	if err != nil {
		return err
	}
	return nil
}