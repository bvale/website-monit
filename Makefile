.PHONY: all

all: build package

build:
	go build -ldflags "-X main.smtpPassword=$(SMTP_PASSWORD)"
package:
	mkdir -p dist
	mv website-monit dist/
	tar -cvf dist/website-monit.tar dist/website-monit
